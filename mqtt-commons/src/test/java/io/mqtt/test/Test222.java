package io.mqtt.test;

import io.mqttpush.mqttclient.Connectioner;
import io.mqttpush.mqttclient.pack.MqttPackExt;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.mqtt.MqttFixedHeader;
import io.netty.handler.codec.mqtt.MqttMessageType;
import io.netty.handler.codec.mqtt.MqttPublishMessage;
import io.netty.handler.codec.mqtt.MqttPublishVariableHeader;
import io.netty.handler.codec.mqtt.MqttQoS;

public class Test222 {

	public static void main(String[] args) throws Exception {
		
		
		Connectioner connectioner=new Connectioner();
		connectioner.setHost("localhost");
		connectioner.setPort(8008);
		connectioner.setUsername("user");
		connectioner.setPassword("user123456");
		connectioner.setDeviceId("root2");
		
		connectioner.initChannel(
				(messagepub)->{
					System.out.println(messagepub);
		},
				(channel)->{
					System.out.println("登录成功");
					
					//connectioner.pubMsg("/root/123", "111".getBytes(), MqttQoS.EXACTLY_ONCE);
					
					
					byte[] bs="aaa".getBytes();
					ByteBuf byteBuf=Unpooled.wrappedBuffer(bs);
					
					int messageid=Math.abs(Integer.valueOf(bs.hashCode()).shortValue());
					MqttFixedHeader mqttFixedHeader=
							new MqttFixedHeader(MqttMessageType.PUBLISH,false, MqttQoS.EXACTLY_ONCE,true , 0);
			 		
			 		MqttPublishVariableHeader variableHeader=
			 				new MqttPublishVariableHeader("/root/12345",messageid);
			 		
			 		MqttPublishMessage mqttPublishMessage=
			 				new MqttPublishMessage(mqttFixedHeader, variableHeader, byteBuf);
			 		
			 		
			 		MqttPackExt mqttPackExt=new MqttPackExt(3, "abc",mqttPublishMessage);

			 		
			 		mqttPackExt.fromIp="192.168.31.120";
			 		mqttPackExt.sport=8080;
			 		channel.writeAndFlush(mqttPackExt);
					
					
		});
		
		
		
		
		
		connectioner.connection().channel().closeFuture().sync();
		
		

	
	
	}

}
