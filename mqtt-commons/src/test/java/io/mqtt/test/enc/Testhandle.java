package io.mqtt.test.enc;

import io.mqttpush.mqttclient.pack.MqttPackExt;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.mqtt.MqttFixedHeader;
import io.netty.handler.codec.mqtt.MqttMessageType;
import io.netty.handler.codec.mqtt.MqttPublishMessage;
import io.netty.handler.codec.mqtt.MqttPublishVariableHeader;
import io.netty.handler.codec.mqtt.MqttQoS;

public class Testhandle extends ChannelInboundHandlerAdapter{

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		super.channelActive(ctx);
		
		
		
		MqttFixedHeader mqttFixedHeader=
				new MqttFixedHeader(MqttMessageType.PUBLISH,false, MqttQoS.AT_LEAST_ONCE,true , 0);
 		
 		MqttPublishVariableHeader variableHeader=
 				new MqttPublishVariableHeader("/root/a111",12345);
 		
 		
 		ByteBuf byteBuf=ctx.alloc().buffer();
 		byteBuf.writeBytes("111".getBytes());
 		
 		
 		MqttPublishMessage mqttPublishMessage=
 				new MqttPublishMessage(mqttFixedHeader, variableHeader,byteBuf );
		
 		
		
		ctx.writeAndFlush(mqttPublishMessage);
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		// TODO Auto-generated method stub
		super.channelRead(ctx, msg);
	}

	
}
