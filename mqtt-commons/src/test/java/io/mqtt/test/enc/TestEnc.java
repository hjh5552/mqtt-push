package io.mqtt.test.enc;

import io.mqttpush.mqttclient.pack.MqttPackExtDecoderByte;
import io.mqttpush.mqttclient.pack.MqttPackExtEncoder;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.mqtt.MqttDecoder;
import io.netty.handler.codec.mqtt.MqttEncoder;

public class TestEnc {

	
	
	public static void main(String[] args) throws InterruptedException {
		
		
		ServerBootstrap bootstrap=new ServerBootstrap();
		
		NioEventLoopGroup eventLoopGroup=new NioEventLoopGroup();
		NioEventLoopGroup childEventLoop=new NioEventLoopGroup();
		
		bootstrap.group(eventLoopGroup, childEventLoop);
		
		bootstrap.channel(NioServerSocketChannel.class);
		
		
		bootstrap.childHandler(new  ChannelInitializer<SocketChannel>() {

			@Override
			protected void initChannel(SocketChannel ch) throws Exception {

			    ch.pipeline()
			    .addLast(MqttEncoder.INSTANCE)
			    //.addLast(new MqttPackExtEncoder())
			    .addLast(new MqttDecoder())
			    .addLast(new MqttPackExtDecoderByte())
			    .addLast(new Testhandle());
				
			}

		
			
			
		});
		
		ChannelFuture channelFuture=bootstrap.bind(12345);
		
		channelFuture.channel().closeFuture().sync();
		
		
		
	}
}
