package io.mqttpush.mqttclient.pack;

import java.util.List;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;

/**
 * 自定义协议的编码器
 * @author tianzhenjiu
 *
 */
public class MqttPackExtEncoder extends MessageToMessageEncoder<MqttPackExt>{

	
	@Override
	protected void encode(ChannelHandlerContext ctx, MqttPackExt msg, List<Object> out) throws Exception {
		
		
	

		ByteBuf byteBuf=ctx.alloc().buffer(64);
		byteBuf.writeInt(MqttPackExt.magicNum);
		
		
		byteBuf.writeInt(msg.fromIp.length());
		
		if(msg.fromIp!=null) {
			byteBuf.writeBytes(msg.fromIp.getBytes());			
		}
		byteBuf.writeInt(msg.sport);
		
		byteBuf.writeInt(msg.headerLen);
		if(msg.headerString!=null) {			
			byteBuf.writeBytes(msg.headerString.getBytes());
		}
		
	
		
		out.add(byteBuf);
		out.add(msg.mqttMsg);

	}

	@Override
	public boolean acceptOutboundMessage(Object msg) throws Exception {
		// TODO Auto-generated method stub
		return super.acceptOutboundMessage(msg);
	}
	

}
