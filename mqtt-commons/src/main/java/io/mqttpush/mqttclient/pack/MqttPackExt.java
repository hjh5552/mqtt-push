package io.mqttpush.mqttclient.pack;

public class MqttPackExt {

	
	/**
	 * 魔数指定为这个类型的消息
	 */
	public static int magicNum=0x12345;

	
	public int fromIpLen;
	
	
	public String fromIp;
	
	
	public int sport;

	
	/**
	 * 指示header的长度
	 */
	public int headerLen;
	
	/**
	 * 指示headerstring的内容，使用:分割键值对，使用,分割组
	 */
	public String headerString;
	
	public Object mqttMsg;
	
	public MqttPackExt() {}

	public MqttPackExt(int headerLen, String headerString, Object mqttMsg) {
		super();
		this.headerLen = headerLen;
		this.headerString = headerString;
		this.mqttMsg = mqttMsg;
	}
	
	
	
	
}
