package io.mqttpush.mqttclient;

import io.netty.handler.codec.mqtt.MqttPublishMessage;

/**
 * 
 * @author acer
 *
 */
@FunctionalInterface
public interface MessageListener {

	/**
	 * 接收到消息处
	 * @param messagepub
	 */
	public void onRecivMessage(MqttPublishMessage messagepub);
}
