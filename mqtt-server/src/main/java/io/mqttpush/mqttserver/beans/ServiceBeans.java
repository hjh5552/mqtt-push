package io.mqttpush.mqttserver.beans;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.mqttpush.mqttserver.service.ChannelUserService;
import io.mqttpush.mqttserver.service.CheckUserService;
import io.mqttpush.mqttserver.service.MessagePushService;
import io.mqttpush.mqttserver.service.TopicManager;
import io.mqttpush.mqttserver.util.thread.SingelThreadPool;


/**
 * 管理用到的service bean
 * @author tianzhenjiu
 *
 */
public class ServiceBeans {


	
	 Logger logger=LoggerFactory.getLogger(getClass());
	
	 
	 /**
	  * 管理着用户与channel的关系
	  */
	 ChannelUserService channelUserService;
	 
	 
	 /**
	  * 检查用户状态
	  */
	 CheckUserService checkUserService;
	 
	 
	 /**
	  * 消息发送
	  */
	 MessagePushService messagePushService;
	 
	 
	 /**
	  * 管理着所有的主题
	  */
	 TopicManager topicManager;
	 
	 
	 
	 /**
	  * 一个使用单线程实现亲缘调度的线程池
	  */
	 SingelThreadPool singleThreadPool;
	 

	 
	 static ServiceBeans serviceBeans;

	 /**
	  * 返回这个单例
	  * @return
	  */
	 public static ServiceBeans getInstance() {
		 
		 if(serviceBeans==null) {
			 serviceBeans=new ServiceBeans();
		 }
		 return serviceBeans;
	 }
	 
	/**
	 * @return the channelUserService
	 */
	public ChannelUserService getChannelUserService() {
		
		if(channelUserService==null) {
			channelUserService=new ChannelUserService();
		}
		return channelUserService;
	}
	/**
	 * @return the checkUserService
	 */
	public CheckUserService getCheckUserService() {
		
		if(checkUserService==null) {
			checkUserService=new CheckUserService();
		}
		return checkUserService;
	}
	/**
	 * @return the messagePushService
	 */
	public MessagePushService getMessagePushService() {
		
		if(messagePushService==null) {
			messagePushService=new MessagePushService();
		}
		return messagePushService;
	}
	/**
	 * @return the topicService
	 */
	public TopicManager getTopicService() {
		
		if(topicManager==null) {
			topicManager=new TopicManager();
		}
		return topicManager;
	}

	/**
	 * 单线程线程池
	 * @return
	 */
	public SingelThreadPool getSingleThreadPool() {
		
		if(singleThreadPool==null) {
			singleThreadPool=SingelThreadPool.getinstance();
		}
	
		return singleThreadPool;
	}

	
	 
	

	
}
